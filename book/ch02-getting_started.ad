= Chapter 2: Getting started with Omnibus GitLab

== Installing Vagrant

Vagrant is what we'll use to build and manage the virtual machine (VM)
environments we'll need to simulate a multi-node GitLab instance.

Follow https://www.vagrantup.com/docs/installation/[installation instructions]
appropriate for your system to get Vagrant installed. In addition, we'll use the
following Vagrant plugins for convenience:

* https://github.com/cogitatio/vagrant-hostsupdater[vagrant-hostsupdater]: `vagrant plugin install vagrant-hostsupdater`
* https://github.com/invernizzi/vagrant-scp[vagrant-scp]: `vagrant plugin install vagrant-scp`
* https://github.com/dotless-de/vagrant-vbguest[vagrant-vbguest]: `vagrant plugin install vagrant-vbguest`

Once we have everything installed, let's define our first VM in a `Vagrantfile`:

.Vagrantfile
[source,ruby]
----
Vagrant.configure('2') do |config|
  config.vm.box = 'debian/buster64'

  # Use vagrant-cachier plugin to cache packages
  # See: https://github.com/fgrehm/vagrant-cachier
  if Vagrant.has_plugin?('vagrant-cachier')
    config.cache.scope = :box
    config.cache.synced_folder_opts = {
      type: :nfs
    }
  end

  config.vm.define 'app1' do |app1|
    app1.vm.network :private_network, ip: '10.200.0.101'
    app1.vm.hostname = 'gitlab-ha.vagrant'
    app1.vm.provider 'virtualbox' do |vb|
      vb.cpus   = 2
      vb.memory = 4096
    end
  end
end
----

Then, run the following command to spin it up:

----
$ vagrant up app1
----

== Installing mkcert and generating a SSL certificate

mkcert is a simple tool for making SSL certificates for use with development or
test systems. It automatically creates and installs a local certificate
authority (CA) in the system root store and generates locally-trusted
certificates, avoiding the trust errors frequently associated with self-signed
certificates.

Follow the https://github.com/FiloSottile/mkcert#installation[installation instructions]
appropriate for your operating system and let's get started creating a SSL
certificate for our GitLab instance:

[source,shell]
----
$ mkcert -install
Using the local CA at "/Users/weimeng/Library/Application Support/mkcert" ✨
The local CA is already installed in the system trust store! 👍
The local CA is already installed in the Firefox trust store! 👍

$ mkcert gitlab-ha.vagrant
Using the local CA at "/Users/weimeng/Library/Application Support/mkcert" ✨

Created a new certificate valid for the following names 📜
 - "gitlab-ha.vagrant"

The certificate is at "./gitlab-ha.vagrant.pem" and the key at "./gitlab-ha.vagrant-key.pem" ✅
----

We'll make use of the `vagrant-scp` plugin we installed earlier to copy our SSL
certificate and key into the `app1` VM so we can use them for our GitLab
instance:

----
$ vagrant scp ./gitlab-ha.vagrant.pem app1:~/gitlab-ha.vagrant.crt
gitlab-ha.vagrant.pem                              100% 1614     1.0MB/s   00:00

$ vagrant scp ./gitlab-ha.vagrant-key.pem app1:~/gitlab-ha.vagrant.key
gitlab-ha.vagrant-key.pem                          100% 1704     1.3MB/s   00:00
----

== Installing GitLab

GitLab is, of course, the star of this book. We'll be using the Omnibus GitLab
Debian package to quickly set up GitLab with minimal dependency installation and
configuration.

[NOTE]
====
Under the hood, Omnibus GitLab uses the https://github.com/chef/omnibus[Chef Omnibus]
framework to provide a full-stack installer for the GitLab web application.
https://github.com/chef/chef[Chef] itself is a Ruby-based configuration
management tool used to automate infrastructure set up. We'll look under the
hood from time to time to see how GitLab's dependencies are installed and
configured.
====

=== Installing the Omnibus GitLab apt package

Follow the instructions to https://about.gitlab.com/install/#debian[install GitLab v12.7.6-ee on Debian]:

[source,shell]
----
$ vagrant ssh app1

vagrant@gitlab-ha:~$ sudo apt-get update
vagrant@gitlab-ha:~$ sudo apt-get install -y curl openssh-server ca-certificates
vagrant@gitlab-ha:~$ curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
vagrant@gitlab-ha:~$ sudo apt-get install gitlab-ee=12.7.6-ee.0
----

Because we did not specify an `EXTERNAL_URL` environment variable, the package
installer did not automatically run a reconfigure of the GitLab environment.
We'll get around to editing the `gitlab.rb` configuration file in just a moment.

=== Installing the self-signed certificate and key

https://docs.gitlab.com/omnibus/settings/nginx.html#manually-configuring-https[Follow the documentation]
and move the SSL certificate and key that we copied to the VM earlier into the
right locations:

[source,shell]
----
vagrant@gitlab-ha:~$ sudo mkdir -p /etc/gitlab/ssl
vagrant@gitlab-ha:~$ sudo mv ~/gitlab-ha.vagrant.crt /etc/gitlab/ssl
vagrant@gitlab-ha:~$ sudo mv ~/gitlab-ha.vagrant.key /etc/gitlab/ssl
vagrant@gitlab-ha:~$ sudo chown -R root:root /etc/gitlab/ssl
----

=== Configuring GitLab

Now, let's edit the `gitlab.rb` configuration file to set the URL we'll access
GitLab by. We'll also go ahead and disable https://letsencrypt.org/[Let's Encrypt]
as we'll be using a self-signed certificate insteadfootnote:[We will not be able
to use Let's Encrypt to generate a certificate in any case as we are not
configuring our VM to be accessible via the internet].

./etc/gitlab/gitlab.rb
[source,ruby]
----
external_url 'https://gitlab-ha.vagrant'
letsencrypt['enabled'] = false
----

Additionally, to faciliate some lab work in a later chapter, we'll configure the
Omnibus-embedded PostgreSQL server to https://docs.gitlab.com/omnibus/settings/database.html#configure-gitlab-rails-block[listen over TCP/IP].

./etc/gitlab/gitlab.rb
[source,ruby]
----
postgresql['listen_address'] = '0.0.0.0'
postgresql['sql_user_password'] = 'f5d3eb3afe9766441ab369c7c103b165' # @gAVAR6E*HkRTn*9
postgresql['md5_auth_cidr_addresses'] = %w(127.0.0.1/24 10.200.0.1/24)
postgresql['trust_auth_cidr_addresses'] = %w(127.0.0.1/24)
----

You can generate the MD5 hash of a user and password for PostgreSQL by using the
built-in GitLab command-line utility:

----
sudo gitlab-ctl pg-password-md5 <postgresql database user, gitlab by default>
----

We'll also configure the GitLab Rails application to connect to the PostgreSQL
server over TCP:

./etc/gitlab/gitlab.rb
[source,ruby]
----
gitlab_rails['db_host'] = '127.0.0.1'
gitlab_rails['db_port'] = 5432
gitlab_rails['db_username'] = 'gitlab'
gitlab_rails['db_password'] = '@gAVAR6E*HkRTn*9'
----

Let's also enable the GitLab Container Registry for use in the next chapter:

./etc/gitlab/gitlab.rb
[source,ruby]
----
registry_external_url = 'https://gitlab-ha.vagrant:5050'
----

Now, complete the install by reconfiguring GitLab (`sudo gitlab-ctl reconfigure`).

=== Basic smoke tests

We should now have a working single-box Omnibus GitLab installation.

Let's first verify that all services are in an `run` state and have been running
for more than a couple of seconds:

[source,shell]
----
vagrant@gitlab-ha:~$ sudo gitlab-ctl status
run: alertmanager: (pid 14594) 1912s; run: log: (pid 14298) 1972s
run: gitaly: (pid 14570) 1913s; run: log: (pid 13732) 2069s
run: gitlab-exporter: (pid 14551) 1914s; run: log: (pid 14161) 1990s
run: gitlab-workhorse: (pid 14523) 1915s; run: log: (pid 14020) 2016s
run: grafana: (pid 15987) 1516s; run: log: (pid 14444) 1930s
run: logrotate: (pid 14061) 2005s; run: log: (pid 14074) 2002s
run: nginx: (pid 19945) 466s; run: log: (pid 14055) 2010s
run: node-exporter: (pid 14532) 1914s; run: log: (pid 14104) 1996s
run: postgres-exporter: (pid 14683) 1912s; run: log: (pid 14344) 1966s
run: postgresql: (pid 13773) 2065s; run: log: (pid 13839) 2064s
run: prometheus: (pid 14563) 1913s; run: log: (pid 14247) 1978s
run: redis: (pid 13604) 2078s; run: log: (pid 13612) 2075s
run: redis-exporter: (pid 14554) 1914s; run: log: (pid 14214) 1984s
run: registry: (pid 20055) 446s; run: log: (pid 18612) 692s
run: sidekiq: (pid 19957) 464s; run: log: (pid 14006) 2021s
run: unicorn: (pid 20410) 402s; run: log: (pid 13978) 2027s
----

Next, let's try accessing our GitLab instance through the browser. Visiting
https://gitlab-ha.vagrant should present you with a prompt to set a password for
the default `root` admin account. Do that, and see if we can log in
successfully.

Next, we'll create a new project named `smoke-test`, initialising the repository
with a README. Now let's do a few basic tests to make sure GitLab is working
as intended:

* Clone and push commits over HTTP:

  $ git clone https://gitlab-ha.vagrant/root/smoke-test.git smoke-test-http
  $ cd smoke-test-http
  $ echo 'This is a test commit, to be pushed over HTTP.\n' >> README.md
  $ git commit -am 'Test HTTP push'
  $ git push

* Clone and push commits over SSH:

  $ git clone git@gitlab-ha.vagrant:root/smoke-test.git smoke-test-ssh
  $ cd smoke-test-ssh
  $ echo 'This is a test commit, to be pushed over SSH.\n' >> README.md
  $ git commit -am 'Test SSH push'
  $ git push

* Login to the GitLab Container Registry:

  $ docker login gitlab-ha.vagrant:5050

* Push an image to the GitLab Container Registry:

  $ docker pull hello-world:latest
  $ docker tag hello-world:latest gitlab-ha.vagrant:5050/root/smoke-test:latest
  $ docker push gitlab-ha.vagrant:5050/root/smoke-test:latest

Peachy keen!

== Breaking GitLab

Let's do what all responsible system administrators should do: Break things!
We'll do this by introducing some load on our GitLab instance by making
concurrent API requests.

We'll first create a personal access token with `api` scope over at
https://gitlab-ha.vagrant/profile/personal_access_tokens.

Now, the fun part: Let's break the GitLab instance! From your host machine, run
the following to make four concurrent API calls to create commits:

[source,shell]
----
$ seq 1 4 | xargs -I{} -n1 -P 4 curl -X POST -H 'Authorization: Bearer j_6q8YdL5Y1Tq5oiiuhR' -H 'Content-Type: application/json' https://gitlab-ha.vagrant/api/v4/projects/1/repository/commits --data '{
  "branch": "master",
  "commit_message": "Test commit {}",
  "actions": [
    {
      "action": "update",
      "file_path": "README.md",
      "content": "Hello GitLab! Are you broken yet?"
    }
  ]
}'
----

Uh oh. You likely saw that nothing happened for about 30 seconds, then one or
more repetitions of the following message appeared:

[source,json]
----
{"message":"4:Deadline Exceeded"}
----

This is the Gitaly client, which GitLab uses to perform Git repository
operations, reporting that we've timed out on a request to the Gitaly server.
This usually appears when we have insufficient I/O to cope with the volume of
writes to disk. But how can that be on our relatively quiet instance?

The truth behind the matter is that we are actually exhausting the number of
Unicorn workers available to serve API requests. But wait, how can that be you
ask? Perhaps you've checked the current processes and found that we have four
Unicorn workers up and ready to serve requests:

[source,shell]
----
vagrant@gitlab-ha:~$ ps -ef | grep unicorn
vagrant   1789  1288  0 19:05 pts/0    00:00:00 grep unicorn
root     32541 32172  0 18:57 ?        00:00:00 runsv unicorn
git      32543 32541  0 18:57 ?        00:00:00 /bin/bash /opt/gitlab/embedded/bin/gitlab-unicorn-wrapper
git      32558     1  8 18:57 ?        00:00:39 unicorn master -D -E production -c /var/opt/gitlab/gitlab-rails/etc/unicorn.rb /opt/gitlab/embedded/service/gitlab-rails/config.ru
root     32563 32541  0 18:57 ?        00:00:00 svlogd -tt /var/log/gitlab/unicorn
git      32747 32558  0 18:58 ?        00:00:03 unicorn worker[0] -D -E production -c /var/opt/gitlab/gitlab-rails/etc/unicorn.rb /opt/gitlab/embedded/service/gitlab-rails/config.ru
git      32748 32558  0 18:58 ?        00:00:01 unicorn worker[1] -D -E production -c /var/opt/gitlab/gitlab-rails/etc/unicorn.rb /opt/gitlab/embedded/service/gitlab-rails/config.ru
git      32749 32558  0 18:58 ?        00:00:02 unicorn worker[2] -D -E production -c /var/opt/gitlab/gitlab-rails/etc/unicorn.rb /opt/gitlab/embedded/service/gitlab-rails/config.ru
git      32750 32558  0 18:58 ?        00:00:02 unicorn worker[3] -D -E production -c /var/opt/gitlab/gitlab-rails/etc/unicorn.rb /opt/gitlab/embedded/service/gitlab-rails/config.ru
----

Surely four workers should be enough to serve four concurrent API requests?

Well, not quite. Each commit creation API request makes a call to the Gitaly
server, which in turn calls the internal `/api/v4/internal/allowed` API endpoint
to verify that the request user has sufficient permissions to write to the
repository.

You probably realised by now where we're going with this: The actual number of
Unicorn processes you'll need to successfully serve four of such commit creation
API requests is actually eight. With only half that number, the pool of
available Unicorn workers is exhausted by the original API requests and the
Gitaly server waits futilely for the follow on internal API request to be
fulfilled, eventually timing out before the API responds.

== Wrap up

There are a couple of ways that GitLab system administrators can migitate the
problem of too many concurrent requests for the number of Unicorn processes to
handle:

* You can scale vertically by increasing the number of Unicorn processes. This
  involves increasing the number of vCPU and RAM available to the GitLab server.

* You can scale horizontally by increasing the number of GitLab nodes. This
  involves increasing the number of GitLab servers to share the load, splitting
  requests evenly across them. This also has the added benefit of adding
  redundancy to the GitLab instance.

Scaling vertically is less complex and easier on maintenance and is generally
the first option most GitLab system administrators will go for. There are, in
fact, organisations hosting GitLab on a beefy machine serving over a thousand
users.

At some point though, you'll either find it impossible to feasibly add more
resources to a single machine or your organisation will have availability
requirements that will dictate a move towards a horizontally-scaled
infrastructure.

This is where our journey towards GitLab High Availability begins.
