= Chapter 7: Setting up a Redis Cluster

In the case of a catastrophic and unrecoverable failure of an application node,
we'll lose some Sidekiq background jobs. This is because each application node's
Sidekiq queues are stored in that node's Redis instance.

To avoid this, we'll first move Redis into a standalone server, then finally
introduce fault tolerance by setting up a Redis cluster with Redis HA and Redis
Sentinel.

#TODO: Add theory#

== Lab: Moving to a standalone Redis server

Easiest way would be to use `redis-cli` to dump to disk, then copy the
`dump.rdb` file into the new server.

----
vagrant@gitlab-app1:~$ sudo /opt/gitlab/embedded/bin/redis-cli -s /var/opt/gitlab/redis/redis.socket

redis /var/opt/gitlab/redis/redis.socket> CONFIG GET dir
1) "dir"
2) "/var/opt/gitlab/redis"
redis /var/opt/gitlab/redis/redis.socket> SAVE
OK
redis /var/opt/gitlab/redis/redis.socket> EXIT

vagrant@gitlab-app1:~$ sudo ls -l /var/opt/gitlab/redis
total 132
-rw------- 1 gitlab-redis gitlab-redis 85233 Feb 29 20:11 dump.rdb
-rw-r--r-- 1 gitlab-redis root         46927 Feb 21 18:56 redis.conf
srwxrwxrwx 1 gitlab-redis gitlab-redis     0 Feb 28 17:11 redis.socket
----

We're not interested in easy. For practice, we're going to set up replication
between two Redis servers so that we can try out manually promoting master.

Let's first reconfigure the `app1` application node to listen to TCP connections
on port `6379`:

./etc/gitlab/gitlab.rb (app1)
[source,ruby]
----
redis['bind'] = '0.0.0.0'
redis['port'] = 6379
redis['password'] 'SuperSecretPassword'
redis['master_password'] = 'SuperSecretPassword'

redis['rename_commands'] = {
  'KEYS': 'KEYS'
}
----

The `KEYS` command is disabled by default as a security measure. We enable it to
have an easy way to inspect what's going on in our Redis server, which we'll go
ahead and do right now:

----
vagrant@gitlab-app1:~$ sudo REDISCLI_AUTH=SuperSecretPassword /opt/gitlab/embedded/bin/redis-cli
127.0.0.1:6379> KEYS *
  1) "resque:gitlab:cron_job:adjourned_group_deletion_worker"
  2) "resque:gitlab:cron_job:ldap_group_sync_worker:enqueued"
  3) "resque:gitlab:cron_job:issue_due_scheduler_worker:enqueued"
  ...
  132) "branch_names:root/test-project:1:set"
  133) "cache:gitlab:flipper/v1/feature/snippets_vue"
  134) "resque:gitlab:cron_job:remove_expired_group_links_worker:enqueued"

127.0.0.1:6379> EXIT
----

Now let's first define our first Redis node and spin it up:

.Vagrantfile
[source,ruby]
----
  config.vm.define 'redis1' do |r1|
    r1.vm.box = 'debian/buster64'
    r1.vm.network :private_network, ip: '10.200.0.51'
    r1.vm.hostname = 'gitlab-redis1.vagrant'

    r1.vm.provider 'virtualbox' do |vb|
      vb.gui    = false
      vb.cpus   = 1
      vb.memory = 256
    end
  end
----

Then, let's configure it:

./etc/gitlab/gitlab.rb (redis1)
[source,ruby]
----
roles ['redis_master_role']

redis['bind'] = '0.0.0.0'
redis['port'] = 6379
redis['password'] 'SuperSecretPassword'

redis['master'] = false
redis['master_name'] = 'gitlab-redis'
redis['master_ip'] = '10.200.10.100'
redis['master_port'] = 6379

redis['rename_commands'] = {
  'KEYS': 'KEYS'
}

gitlab_rails['auto_migrate'] = false
----

Easy. Let's take a look using `redis-cli`:

----
vagrant@gitlab-redis1:~$ sudo REDISCLI_AUTH=SuperSecretPassword /opt/gitlab/embedded/bin/redis-cli
127.0.0.1:6379> INFO replication
# Replication
role:slave
master_host:10.200.0.100
master_port:6379
master_link_status:up
master_last_io_seconds_ago:3
master_sync_in_progress:0
slave_repl_offset:133856
slave_priority:100
slave_read_only:1
connected_slaves:0
master_replid:d76d73c5d747d05ea9949c8bce381d9f2c609a3c
master_replid2:0000000000000000000000000000000000000000
master_repl_offset:133856
second_repl_offset:-1
repl_backlog_active:1
repl_backlog_size:1048576
repl_backlog_first_byte_offset:1
repl_backlog_histlen:133856

127.0.0.1:6379> KEYS *
  1) "resque:gitlab:cron_job:import_software_licenses_worker"
  2) "cache:gitlab:geo:node_enabled:12.7.6-ee:5.2.3"
  3) "resque:gitlab:cron_job:remove_expired_members_worker:enqueued"
  ...
  131) "resque:gitlab:cron_job:container_expiration_policy_worker"
  132) "resque:gitlab:cron_job:issue_due_scheduler_worker:enqueued"
  133) "resque:gitlab:cron_job:gitlab_usage_ping_worker"
----

Over before you even knew it!

Now, let's promote our `redis1` Redis into master.

./etc/gitlab/gitlab.rb (redis1)
[sub="quotes"]
[source,ruby]
----
roles ['redis_master_role']

redis['bind'] = '0.0.0.0'
redis['port'] = 6379
redis['password'] 'SuperSecretPassword'

*redis['master'] = true* # Or comment out this line
*# redis['master_name'] = 'gitlab-redis'
# redis['master_ip'] = '10.200.10.100'
# redis['master_port'] = 6379*

redis['rename_commands'] = {
  'KEYS': 'KEYS'
}

gitlab_rails['auto_migrate'] = false
----

And reconfigure. Let's verify that our node is now master:

----
vagrant@gitlab-redis1:~$ sudo REDISCLI_AUTH=SuperSecretPassword /opt/gitlab/embedded/bin/redis-cli
127.0.0.1:6379> INFO replication
# Replication
role:master
connected_slaves:0
master_replid:bfd6953db2384e80ebd16b63f2179de7c3ef0b5a
master_replid2:0000000000000000000000000000000000000000
master_repl_offset:0
second_repl_offset:-1
repl_backlog_active:0
repl_backlog_size:1048576
repl_backlog_first_byte_offset:0
repl_backlog_histlen:0
----

And the final step would be to connect our app nodes to `redis1`:

./etc/gitlab/gitlab.rb (app1, app2)
[source,ruby]
----
redis['enable'] = false

gitlab_rails['redis_host'] = '10.200.0.51'
gitlab_rails['redis_port'] = 6379
gitlab_rails['redis_password'] = 'SuperSecretPassword'
----

And we're done here.

== Lab: Moving to a Redis cluster

Let's get our two replica Redis nodes up and running:

.Vagrantfile
[source,ruby]
----
  config.vm.define 'redis2' do |r2|
    r2.vm.box = 'debian/buster64'
    r2.vm.network :private_network, ip: '10.200.0.52'
    r2.vm.hostname = 'gitlab-redis2.vagrant'

    r2.vm.provider 'virtualbox' do |vb|
      vb.gui    = false
      vb.cpus   = 1
      vb.memory = 256
    end
  end

  config.vm.define 'redis3' do |r3|
    r3.vm.box = 'debian/buster64'
    r3.vm.network :private_network, ip: '10.200.0.53'
    r3.vm.hostname = 'gitlab-redis3.vagrant'

    r3.vm.provider 'virtualbox' do |vb|
      vb.gui    = false
      vb.cpus   = 1
      vb.memory = 256
    end
  end
----

Install GitLab and configure them:

./etc/gitlab/gitlab.rb (redis2, redis3)
[source,ruby]
----
# external_url 'http://gitlab.example.com'

roles['redis_slave_role']

redis['bind'] = '0.0.0.0'
redis['port'] = 6379
redis['password'] = 'SuperSecretPassword'
redis['master_ip'] = '10.200.0.51'

gitlab_rails['auto_migrate'] = false
----

Verify that they are replicas:

----
vagrant@gitlab-redis3:~$ sudo REDISCLI_AUTH=SuperSecretPassword /opt/gitlab/embedded/bin/redis-cli
127.0.0.1:6379> INFO replication
# Replication
role:slave
master_host:10.200.0.51
master_port:6379
master_link_status:up
master_last_io_seconds_ago:2
master_sync_in_progress:0
slave_repl_offset:2381055
slave_priority:100
slave_read_only:1
connected_slaves:0
master_replid:f231a4ca6302cb825b636285614a477d2d630145
master_replid2:0000000000000000000000000000000000000000
master_repl_offset:2381055
second_repl_offset:-1
repl_backlog_active:1
repl_backlog_size:1048576
repl_backlog_first_byte_offset:1332480
repl_backlog_histlen:1048576
----

Done! That was easy. Now for some chaos.

== Lab: What happens when master goes down?

----
vagrant@gitlab-redis1:~$ sudo gitlab-ctl stop redis
ok: down: redis: 0s, normally up
----

Boom! We get a 500 on our GitLab instance right away.

Let's have a look at what's going on in one of our Redis replica nodes. Let's
pick `redis3`:

----
vagrant@gitlab-redis3:~$ sudo REDISCLI_AUTH=SuperSecretPassword /opt/gitlab/embedded/bin/redis-cli
127.0.0.1:6379> INFO replication
# Replication
role:slave
master_host:10.200.0.51
master_port:6379
master_link_status:down
master_last_io_seconds_ago:-1
master_sync_in_progress:0
slave_repl_offset:2956142
master_link_down_since_seconds:400
slave_priority:100
slave_read_only:1
connected_slaves:0
master_replid:f231a4ca6302cb825b636285614a477d2d630145
master_replid2:0000000000000000000000000000000000000000
master_repl_offset:2956142
second_repl_offset:-1
repl_backlog_active:1
repl_backlog_size:1048576
repl_backlog_first_byte_offset:1907567
repl_backlog_histlen:1048576
----

We can see that `master_link_status` is `down`. Let's go ahead and elect this
replica as master.

----
127.0.0.1:6379> REPLICAOF NO ONE
OK

127.0.0.1:6379> INFO replication
# Replication
role:master
connected_slaves:0
master_replid:e01bf0afb79ed7d83c9d45baee41f4ea91093959
master_replid2:f231a4ca6302cb825b636285614a477d2d630145
master_repl_offset:2956577
second_repl_offset:2956143
repl_backlog_active:1
repl_backlog_size:1048576
repl_backlog_first_byte_offset:1908002
repl_backlog_histlen:1048576
----

Now let's look at what's going on with `redis2`:

----
vagrant@gitlab-redis2:~$ sudo REDISCLI_AUTH=SuperSecretPassword /opt/gitlab/embedded/bin/redis-cli
127.0.0.1:6379> INFO replication
# Replication
role:slave
master_host:10.200.0.51
master_port:6379
master_link_status:down
master_last_io_seconds_ago:-1
master_sync_in_progress:0
slave_repl_offset:2956142
master_link_down_since_seconds:565
slave_priority:100
slave_read_only:1
connected_slaves:0
master_replid:f231a4ca6302cb825b636285614a477d2d630145
master_replid2:0000000000000000000000000000000000000000
master_repl_offset:2956142
second_repl_offset:-1
repl_backlog_active:1
repl_backlog_size:1048576
repl_backlog_first_byte_offset:1907567
repl_backlog_histlen:1048576
----

Let's get `redis2` to follow our new master:

----
127.0.0.1:6379> REPLICAOF 10.200.0.53 6379
OK

127.0.0.1:6379> INFO replication
# Replication
role:slave
master_host:10.200.0.53
master_port:6379
master_link_status:up
master_last_io_seconds_ago:1
master_sync_in_progress:0
slave_repl_offset:2956591
slave_priority:100
slave_read_only:1
connected_slaves:0
master_replid:e01bf0afb79ed7d83c9d45baee41f4ea91093959
master_replid2:f231a4ca6302cb825b636285614a477d2d630145
master_repl_offset:2956591
second_repl_offset:2956143
repl_backlog_active:1
repl_backlog_size:1048576
repl_backlog_first_byte_offset:1908016
repl_backlog_histlen:1048576
----

Finally, let's adjust the settings on our application nodes:

./etc/gitlab/gitlab.rb
[source,ruby]
----
gitlab_rails['redis_host'] = '10.200.0.53'
----

Reconfigure and we're back in action! Let's restore `redis1`:

----
vagrant@gitlab-redis1:~$ sudo gitlab-ctl start redis
ok: run: redis: (pid 13469) 0s

vagrant@gitlab-redis1:~$ sudo REDISCLI_AUTH=SuperSecretPassword /opt/gitlab/embedded/bin/redis-cli

127.0.0.1:6379> INFO replication
# Replication
role:master
connected_slaves:0
master_replid:12297f0f502d9d326e96542ba75a5d45d4c669bb
master_replid2:0000000000000000000000000000000000000000
master_repl_offset:0
second_repl_offset:-1
repl_backlog_active:0
repl_backlog_size:1048576
repl_backlog_first_byte_offset:0
repl_backlog_histlen:0
----

Our original master node still thinks they are the master. Time to put it back
in their place.

----
127.0.0.1:6379> REPLICAOF 10.200.0.53 6379
OK
127.0.0.1:6379> INFO replication
# Replication
role:slave
master_host:10.200.0.53
master_port:6379
master_link_status:up
master_last_io_seconds_ago:2
master_sync_in_progress:0
slave_repl_offset:3955912
slave_priority:100
slave_read_only:1
connected_slaves:0
master_replid:e01bf0afb79ed7d83c9d45baee41f4ea91093959
master_replid2:0000000000000000000000000000000000000000
master_repl_offset:3955912
second_repl_offset:-1
repl_backlog_active:1
repl_backlog_size:1048576
repl_backlog_first_byte_offset:3955913
repl_backlog_histlen:0
----

And now we're done.

I don't know about you, but I'm not going to wake up at 3am in the morning
bleary-eyed after receiving a page. Let's look at how we can automate the Redis
failover and application node cut-over process with Redis Sentinel.

= Lab: Setting up Redis Sentinel

Let's put our Redis Sentinels on the monitor nodes we created earlier for
Consul.

./etc/gitlab/gitlab.rb (monitor1, monitor2, monitor3)
[sub="quotes"]
[source,ruby]
----
roles ['consul_role', 'redis_sentinel_role']

redis['master_name'] = 'gitlab-redis'
redis['master_ip'] = '10.200.0.53'
redis['master_port'] = 6379
redis['master_password'] = 'SuperSecretPassword'

sentinel['quorum'] = 2
----

Reconfigure and Sentinel should be up. Let's check the logs:

----
vagrant@gitlab-monitor1:~$ sudo tail -10 /var/log/gitlab/sentinel/current
2020-03-04_15:13:04.61725 18049:X 04 Mar 2020 15:13:04.617 # oO0OoO0OoO0Oo Redis is starting oO0OoO0OoO0Oo
2020-03-04_15:13:04.61728 18049:X 04 Mar 2020 15:13:04.617 # Redis version=5.0.7, bits=64, commit=4891612b, modified=0, pid=18049, just started
2020-03-04_15:13:04.61729 18049:X 04 Mar 2020 15:13:04.617 # Configuration loaded
2020-03-04_15:13:04.61925 18049:X 04 Mar 2020 15:13:04.619 * Running mode=sentinel, port=26379.
2020-03-04_15:13:04.61930 18049:X 04 Mar 2020 15:13:04.619 # WARNING: The TCP backlog setting of 511 cannot be enforced because /proc/sys/net/core/somaxconn is set to the lower value of 128.
2020-03-04_15:13:04.61942 18049:X 04 Mar 2020 15:13:04.619 # Sentinel ID is 0967595b67df0ffea73bf498b60b0ddfe0b994e0
2020-03-04_15:13:04.61944 18049:X 04 Mar 2020 15:13:04.619 # +monitor master gitlab-redis 10.200.0.53 6379 quorum 2
2020-03-04_15:13:04.62198 18049:X 04 Mar 2020 15:13:04.621 * +slave slave 10.200.0.52:6379 10.200.0.52 6379 @ gitlab-redis 10.200.0.53 6379
2020-03-04_15:13:04.62283 18049:X 04 Mar 2020 15:13:04.622 * +slave slave 10.200.0.51:6379 10.200.0.51 6379 @ gitlab-redis 10.200.0.53 6379
----

Sentinel uses its configuration file to save the current state that will be
reloaded in case of restarts. Let's see what we have in the configuration file:

----
vagrant@gitlab-monitor1:~$ sudo cat /var/opt/gitlab/sentinel/sentinel.conf
# This file is managed by gitlab-ctl. Manual changes will be
# erased! To change the contents below, edit /etc/gitlab/gitlab.rb
# and run `sudo gitlab-ctl reconfigure`.

...

sentinel monitor gitlab-redis 10.200.0.53 6379 2

...

# Generated by CONFIG REWRITE
protected-mode no
sentinel auth-pass gitlab-redis SuperSecretPassword
sentinel config-epoch gitlab-redis 0
sentinel leader-epoch gitlab-redis 0
sentinel known-replica gitlab-redis 10.200.0.51 6379
sentinel known-replica gitlab-redis 10.200.0.52 6379
sentinel known-sentinel gitlab-redis 10.200.0.43 26379 1c15474587ec2f86d0b2c94c9b90b60ebe3ff114
sentinel known-sentinel gitlab-redis 10.200.0.42 26379 6d3eb357fba16093287a56687418c2ae3fe531f1
sentinel current-epoch 0
----

We can also inspect Sentinel by using `redis-cli`. Sentinel is basically
`redis-server` running in Sentinel mode.

----
vagrant@gitlab-monitor1:~$ sudo /opt/gitlab/embedded/bin/redis-cli -p 26379

127.0.0.1:26379> INFO server
# Server
redis_version:5.0.7
redis_git_sha1:4891612b
redis_git_dirty:0
redis_build_id:8363000cb7c0f6d2
redis_mode:sentinel
os:Linux 4.19.0-8-amd64 x86_64
arch_bits:64
multiplexing_api:epoll
atomicvar_api:atomic-builtin
gcc_version:8.3.0
process_id:479
run_id:dddf5d4f5d997dae951ffdb14c96c7cd9647a203
tcp_port:26379
uptime_in_seconds:2059
uptime_in_days:0
hz:13
configured_hz:10
lru_clock:6285609
executable:/opt/gitlab/embedded/bin/redis-sentinel
config_file:/var/opt/gitlab/sentinel/sentinel.conf
----

We can also get some basic information about the Sentinel cluster:

----
127.0.0.1:26379> INFO sentinel
# Sentinel
sentinel_masters:1
sentinel_tilt:0
sentinel_running_scripts:0
sentinel_scripts_queue_length:0
sentinel_simulate_failure_flags:0
master0:name=gitlab-redis,status=ok,address=10.200.0.52:6379,slaves=2,sentinels=3
----

We can also query the state of the current master

----
127.0.0.1:26379> SENTINEL master gitlab-redis
 1) "name"
 2) "gitlab-redis"
 3) "ip"
 4) "10.200.0.52"
 5) "port"
 6) "6379"
 7) "runid"
 8) "b9fd67716773d04e9b23b3c294dc02b2819a82d0"
 9) "flags"
10) "master"
11) "link-pending-commands"
12) "0"
13) "link-refcount"
14) "1"
15) "last-ping-sent"
16) "0"
17) "last-ok-ping-reply"
18) "296"
19) "last-ping-reply"
20) "296"
21) "down-after-milliseconds"
22) "10000"
23) "info-refresh"
24) "4000"
25) "role-reported"
26) "master"
27) "role-reported-time"
28) "1952147"
29) "config-epoch"
30) "4"
31) "num-slaves"
32) "2"
33) "num-other-sentinels"
34) "2"
35) "quorum"
36) "2"
37) "failover-timeout"
38) "60000"
39) "parallel-syncs"
40) "1"
----

- `num-other-sentinels` is `2`, so we know Sentinel already detected two more
Sentinels for this master.
- `flags` is just `master`. If master was down, we'd see `s_down` or `o_down`
flag as well
- `num-slaves` is set to `2`, so Sentinel also detected we have two replicas
attached to our master.

We can also do `SENTINEL replicas gitlab-redis` and `SENTINEL sentinels gitlab-redis`
to get more information about other sentinels.

Get current master:

----
127.0.0.1:26379> SENTINEL get-master-addr-by-name gitlab-redis
1) "10.200.0.52"
2) "6379"
----

More commands: https://redis.io/topics/sentinel#sentinel-api

Now let's reconfigure our app nodes:

./etc/gitlab/gitlab.rb (app1, app2)
[source,ruby]
----
# gitlab_rails['redis_host'] = '10.200.0.51'
# gitlab_rails['redis_port'] = 6379
# gitlab_rails['redis_password'] = 'SuperSecretPassword'

redis['master_name'] = 'gitlab-redis'
redis['master_password'] = 'SuperSecretPassword'

gitlab_rails['redis_sentinels'] = [
  {'host' => '10.200.0.41', 'port' => 26379},
  {'host' => '10.200.0.42', 'port' => 26379},
  {'host' => '10.200.0.43', 'port' => 26379}
]
----

Under the hood, GitLab uses the Redis Ruby client library to
https://github.com/redis/redis-rb#sentinel-support[connect to Redis Sentinel].
It connects to the master by default. https://github.com/redis/redis-rb/blob/master/lib/redis/client.rb#L541

Behind the scenes, the library uses `SENTINEL get-master-addr-by-name gitlab-redis`
to get the current master. https://github.com/redis/redis-rb/blob/master/lib/redis/client.rb#L601-L607

#TODO: Update redis-rb links to use the version which v12.7-ee uses#

Let's simulate a temporary failure of the Redis master by putting it to sleep
for 20 seconds.

----
vagrant@gitlab-monitor1:~$ sudo REDISCLI_AUTH=SuperSecretPassword /opt/gitlab/embedded/bin/redis-cli -h 10.200.0.52  -p 6379 DEBUG sleep 20

vagrant@gitlab-monitor1:~$ sudo /opt/gitlab/embedded/bin/redis-cli -p 26379

127.0.0.1:26379> SENTINEL get-master-addr-by-name gitlab-redis
1) "10.200.0.51"
2) "6379"
----

We see that the master has changed and our application seamlessly switches over
to using that, thanks to the Redis Ruby client talking to the Sentinels we
defined and determining which is the master.

Here's what the failover looks like in the logs of the Sentinel elected to
conduct the failover:

----
2020-03-04_18:09:50.80188 479:X 04 Mar 2020 18:09:50.801 # +sdown master gitlab-redis 10.200.0.51 6379
2020-03-04_18:09:50.85530 479:X 04 Mar 2020 18:09:50.855 # +odown master gitlab-redis 10.200.0.51 6379 #quorum 2/2
----

Redis Sentinel has two different concepts of down. `sdown` means Subjectively
Down and means that master is down from the point of view of this Sentinel.
`odown` means Objectively Down and means that enough Sentinels have observed
master to be down. Enough is the defined quorum, and we can see this as
`#quorum 2/2`

----
2020-03-04_18:09:50.85556 479:X 04 Mar 2020 18:09:50.855 # +new-epoch 6
----

This means that a new configuration epoch has started. Every failover is
versioned with a unique number. https://redis.io/topics/sentinel#configuration-epochs
In practical terms, this means that a failover event has occured and the
Sentinels are attempting to elect a new master.

----
2020-03-04_18:09:50.85579 479:X 04 Mar 2020 18:09:50.855 # +try-failover master gitlab-redis 10.200.0.51 6379
----

Failover initiatiated for `10.200.0.51 6379` (current master) and election will
take fail shortly.

----
2020-03-04_18:09:50.85726 479:X 04 Mar 2020 18:09:50.857 # +vote-for-leader 6d3eb357fba16093287a56687418c2ae3fe531f1 6
2020-03-04_18:09:50.85986 479:X 04 Mar 2020 18:09:50.859 # 1c15474587ec2f86d0b2c94c9b90b60ebe3ff114 voted for 6d3eb357fba16093287a56687418c2ae3fe531f1 6
2020-03-04_18:09:50.86023 479:X 04 Mar 2020 18:09:50.860 # 0967595b67df0ffea73bf498b60b0ddfe0b994e0 voted for 6d3eb357fba16093287a56687418c2ae3fe531f1 6
2020-03-04_18:09:50.91621 479:X 04 Mar 2020 18:09:50.916 # +elected-leader master gitlab-redis 10.200.0.51 6379
----

Sentinels are voting to see who should conduct the failover. This Sentinel voted
for itself and won unanimously!

----
2020-03-04_18:09:50.91663 479:X 04 Mar 2020 18:09:50.916 # +failover-state-select-slave master gitlab-redis 10.200.0.51 6379
----

Trying to find a suitable replicas for promotion.

----
2020-03-04_18:09:50.98844 479:X 04 Mar 2020 18:09:50.988 # +selected-slave slave 10.200.0.52:6379 10.200.0.52 6379 @ gitlab-redis 10.200.0.51 6379
----

Found a suitable candidate.

----
2020-03-04_18:09:50.98873 479:X 04 Mar 2020 18:09:50.988 * +failover-state-send-slaveof-noone slave 10.200.0.52:6379 10.200.0.52 6379 @ gitlab-redis 10.200.0.51 6379
----

This should be familiar. Leader Sentinel is sending the `SLAVEOF NO ONE` command
to promote the selected replica as master.

----
2020-03-04_18:09:51.04218 479:X 04 Mar 2020 18:09:51.042 * +failover-state-wait-promotion slave 10.200.0.52:6379 10.200.0.52 6379 @ gitlab-redis 10.200.0.51 6379
----

Waiting...

----
2020-03-04_18:09:51.88464 479:X 04 Mar 2020 18:09:51.884 # +promoted-slave slave 10.200.0.52:6379 10.200.0.52 6379 @ gitlab-redis 10.200.0.51 6379
----

Promoted!

----
2020-03-04_18:09:51.88484 479:X 04 Mar 2020 18:09:51.884 # +failover-state-reconf-slaves master gitlab-redis 10.200.0.51 6379
----

Failover state changed to `reconf-slaves` as we now move on to reconfigure the
new replicas

----
2020-03-04_18:09:51.93834 479:X 04 Mar 2020 18:09:51.938 * +slave-reconf-sent slave 10.200.0.53:6379 10.200.0.53 6379 @ gitlab-redis 10.200.0.51 6379
2020-03-04_18:09:52.89843 479:X 04 Mar 2020 18:09:52.898 * +slave-reconf-inprog slave 10.200.0.53:6379 10.200.0.53 6379 @ gitlab-redis 10.200.0.51 6379
2020-03-04_18:09:52.89868 479:X 04 Mar 2020 18:09:52.898 * +slave-reconf-done slave 10.200.0.53:6379 10.200.0.53 6379 @ gitlab-redis 10.200.0.51 6379
----

Reconfigure of our remaining replica to follow the new master is done.

----
2020-03-04_18:09:52.96886 479:X 04 Mar 2020 18:09:52.968 # -odown master gitlab-redis 10.200.0.51 6379
2020-03-04_18:09:52.96941 479:X 04 Mar 2020 18:09:52.969 # +failover-end master gitlab-redis 10.200.0.51 6379
----

Instance is no longer objectively down and failover state has terminated
successfully -- all replicas appear to have been reconfigured to replicate with
the new master.

----
2020-03-04_18:09:52.97000 479:X 04 Mar 2020 18:09:52.969 # +switch-master gitlab-redis 10.200.0.51 6379 10.200.0.52 6379
----

Possibly the most important line. Here we log that the master for `gitlab-redis`
has changed from `10.200.0.51:6379` to `10.200.0.52:6379`.

----
2020-03-04_18:09:52.97160 479:X 04 Mar 2020 18:09:52.971 * +slave slave 10.200.0.53:6379 10.200.0.53 6379 @ gitlab-redis 10.200.0.52 6379
2020-03-04_18:09:52.97213 479:X 04 Mar 2020 18:09:52.972 * +slave slave 10.200.0.51:6379 10.200.0.51 6379 @ gitlab-redis 10.200.0.52 6379
----

New replicas were detected and attached to the master.

And here's what the logs look like from a non-leader Sentinel:

----
2020-03-04_18:09:50.84315 479:X 04 Mar 2020 18:09:50.842 # +new-epoch 6
2020-03-04_18:09:50.84352 479:X 04 Mar 2020 18:09:50.843 # +vote-for-leader 6d3eb357fba16093287a56687418c2ae3fe531f1 6
2020-03-04_18:09:50.96030 479:X 04 Mar 2020 18:09:50.960 # +sdown master gitlab-redis 10.200.0.51 6379
2020-03-04_18:09:51.01297 479:X 04 Mar 2020 18:09:51.012 # +odown master gitlab-redis 10.200.0.51 6379 #quorum 3/2
2020-03-04_18:09:51.01302 479:X 04 Mar 2020 18:09:51.013 # Next failover delay: I will not start a failover before Wed Mar  4 18:11:51 2020
----

We see this message because once a Sentinel has voted for another Sentinel for
the failover of a given master, it will wait some time before attempting to
failover the same master again. This delay can be configured in `failover-timeout`.
#TODO: Find the gitlab.rb setting#

----
2020-03-04_18:09:51.92424 479:X 04 Mar 2020 18:09:51.922 # +config-update-from sentinel 6d3eb357fba16093287a56687418c2ae3fe531f1 10.200.0.42 26379 @ gitlab-redis 10.200.0.51 6379
----

We're receiving a configuration update from the leader Sentinel

----
2020-03-04_18:09:51.92444 479:X 04 Mar 2020 18:09:51.922 # +switch-master gitlab-redis 10.200.0.51 6379 10.200.0.52 6379
2020-03-04_18:09:51.92445 479:X 04 Mar 2020 18:09:51.923 * +slave slave 10.200.0.53:6379 10.200.0.53 6379 @ gitlab-redis 10.200.0.52 6379
2020-03-04_18:09:51.92445 479:X 04 Mar 2020 18:09:51.923 * +slave slave 10.200.0.51:6379 10.200.0.51 6379 @ gitlab-redis 10.200.0.52 6379
----

We've seen this before, nothing new here.

----
2020-03-04_18:10:10.07037 479:X 04 Mar 2020 18:10:10.070 * +convert-to-slave slave 10.200.0.51:6379 10.200.0.51 6379 @ gitlab-redis 10.200.0.52 6379
----

The old master is converted to a slave. Note that this only happens on
one of the Sentinels.

Let's now see what happens when two of our Redis Sentinels are down and Redis
master goes down.

----
2020-03-04_19:03:50.01478 479:X 04 Mar 2020 19:03:50.014 # +sdown sentinel 1c15474587ec2f86d0b2c94c9b90b60ebe3ff114 10.200.0.43 26379 @ gitlab-redis 10.200.0.53 6379
2020-03-04_19:04:02.88030 479:X 04 Mar 2020 19:04:02.880 # +sdown sentinel 6d3eb357fba16093287a56687418c2ae3fe531f1 10.200.0.42 26379 @ gitlab-redis 10.200.0.53 6379
----

Sentinels are detected as down.

----
2020-03-04_19:05:23.31161 479:X 04 Mar 2020 19:05:23.311 # +sdown master gitlab-redis 10.200.0.53 6379
----

And that's it. Because there's no quorum, there cannot be an objective down
state.

Let's start one of our Sentinels and see what happens:

----
2020-03-04_19:07:32.01549 479:X 04 Mar 2020 19:07:32.015 # -sdown sentinel 6d3eb357fba16093287a56687418c2ae3fe531f1 10.200.0.42 26379 @ gitlab-redis 10.200.0.53 6379
2020-03-04_19:07:41.15518 479:X 04 Mar 2020 19:07:41.155 # +new-epoch 9
2020-03-04_19:07:41.15731 479:X 04 Mar 2020 19:07:41.157 # +vote-for-leader 6d3eb357fba16093287a56687418c2ae3fe531f1 9
----

Sentinel comes back up, and is voted as the new leader.

----
2020-03-04_19:07:41.33380 479:X 04 Mar 2020 19:07:41.333 # +odown master gitlab-redis 10.200.0.53 6379 #quorum 2/2
----

With quorum met, Redis master can now be seen as objectively down.

----
2020-03-04_19:07:41.33403 479:X 04 Mar 2020 19:07:41.334 # Next failover delay: I will not start a failover before Wed Mar  4 19:09:41 2020
2020-03-04_19:07:42.28097 479:X 04 Mar 2020 19:07:42.280 # +config-update-from sentinel 6d3eb357fba16093287a56687418c2ae3fe531f1 10.200.0.42 26379 @ gitlab-redis 10.200.0.53 6379
2020-03-04_19:07:42.28160 479:X 04 Mar 2020 19:07:42.281 # +switch-master gitlab-redis 10.200.0.53 6379 10.200.0.52 6379
2020-03-04_19:07:42.28244 479:X 04 Mar 2020 19:07:42.282 * +slave slave 10.200.0.51:6379 10.200.0.51 6379 @ gitlab-redis 10.200.0.52 6379
2020-03-04_19:07:42.28276 479:X 04 Mar 2020 19:07:42.282 * +slave slave 10.200.0.53:6379 10.200.0.53 6379 @ gitlab-redis 10.200.0.52 6379
2020-03-04_19:07:52.29498 479:X 04 Mar 2020 19:07:52.294 # +sdown slave 10.200.0.53:6379 10.200.0.53 6379 @ gitlab-redis 10.200.0.52 6379
----

Let's restart our downed Redis master

----
2020-03-04_19:10:52.18788 479:X 04 Mar 2020 19:10:52.187 # -sdown slave 10.200.0.53:6379 10.200.0.53 6379 @ gitlab-redis 10.200.0.52 6379
2020-03-04_19:11:02.15202 479:X 04 Mar 2020 19:11:02.151 * +convert-to-slave slave 10.200.0.53:6379 10.200.0.53 6379 @ gitlab-redis 10.200.0.52 6379
----

And we're back.

Short discussion on Sentinel split-brain:
https://stackoverflow.com/questions/48629944/how-redis-with-sentinels-behave-after-split-brain
